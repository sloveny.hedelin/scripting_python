#!/usr/bin/env python
#-*- coding: utf-8 -*-

import re
import argparse
import os

def extract_str(args):
    input_regex = re.compile(r"\"(.*)\"|\'(.*)\'")
    for root, _, filenames in os.walk(args.directory):
        for filename in filenames:
            if not args.suffix:
                continue
            if not args.all and line.startswith ("."):
                continue
        filepath = os.path.join(root, filename)
        try:
            with open (filepath, encoding="utf8", errors ="ignore") as file:
                for line in file:
                    for match in input_regex.finditer(line):
                        if args.path:
                            print(filepath," : ", match.group(0))
                        else:
                            print(match.group(0))
        except:
            pass             
def main():
    parser = argparse.ArgumentParser(description='Extraction of all string type')
    parser.add_argument('directory',type=str, help= "Beginning of your directory")
    parser.add_argument('--suffix', type= str, help="Search for suffix")
    parser.add_argument('--path', help="The path to the current file", action='store_true')
    parser.add_argument('--all', help="List all hidden folders", action= 'store_true')
    args = parser.parse_args()
    extract_str(args)
    
if __name__ == "__main__":
    main()