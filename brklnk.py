#!/usr/bin/env python
#-*- coding: utf-8 -*
import requests
import argparse
from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc, 'html.parser')

#soup.find(id="link3")


def breaklinks():
    r = requests.get('https://educ-a-dom.fr/brklnk')
    for link in soup.find_all('a'):
        print(link.get('href'))



def main():
    parser = argparse.ArgumentParser(description='Get all broken links')
    parser.add_argument('help',type=str, help= "How to use the command")
    parser.add_argument('depth =n', action='store_true', help="Search depth = n")
    args = parser.parse_args()
    breaklinks(args)

if __name__ == "__main__":
    main(